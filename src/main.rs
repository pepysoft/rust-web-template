use actix_files as fs;
use actix_web::{HttpServer, App, web, HttpResponse, Responder};
use tera::{Tera, Context};


async fn index(tera: web::Data<Tera>) -> impl Responder {
    let mut data = Context::new();
    data.insert("title", "Blog ");
    data.insert("name","do Xande");

    let rendered = tera.render("index.html", &data).unwrap();
    HttpResponse::Ok().body(rendered)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        let tera = Tera::new("templates/**/*").unwrap();
        App::new()
            .data(tera)
            .service(fs::Files::new("/static", "static/").show_files_listing())
            .route("/", web::get().to(index))
    })
    .bind("127.0.0.1:7000")?
    .run()
    .await
}